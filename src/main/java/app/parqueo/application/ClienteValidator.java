package app.parqueo.application;

import app.parqueo.domain.model.Cliente;
import app.parqueo.domain.model.ResponseInsertarCliente;

public class ClienteValidator {
	
	public ResponseInsertarCliente InsertarCliente(Cliente cliente)
	{
		
		ResponseInsertarCliente response = new ResponseInsertarCliente();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);	
		
		if(cliente.getClie_TokenFirebase() == null) {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Token vacío o nulo");
		}

		return response;
	}
	
}
