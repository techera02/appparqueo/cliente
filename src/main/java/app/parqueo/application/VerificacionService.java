package app.parqueo.application;

import org.springframework.stereotype.Service;

import app.parqueo.domain.persistence_ports.VerificacionPersistence;


@Service
public class VerificacionService 
{
	VerificacionPersistence verificacionPersistence;
	
	public VerificacionService(VerificacionPersistence verificacionPersistence)
	{
		this.verificacionPersistence = verificacionPersistence;
	}
	
	public Integer insertarVerificacion(String nroTelefono) 
	{
        return this.verificacionPersistence.insertarVerificacion(nroTelefono);
    }
	
	public Integer validarCodigoVerificacion(String codigo, String nroTelefono) 
	{
        return this.verificacionPersistence.validarCodigoVerificacion(codigo, nroTelefono);
    }
	
}
