package app.parqueo.application;

import app.parqueo.domain.model.ResponseValidarCodigoVerificacion;
import app.parqueo.domain.model.ResponseValidarNumeroTelefono;

public class VerificacionValidator 
{

	public ResponseValidarNumeroTelefono ValidarNumeroTelefonoCliente(String nroTelefono)
	{
		
		ResponseValidarNumeroTelefono response = new ResponseValidarNumeroTelefono();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);	
		
		try 
		{
			Integer.parseInt(nroTelefono);
			
			if(nroTelefono.length()!=9)
			{
				response.setCodError(2);
				response.setEstado(false);
				response.setMensaje("El número del teléfono no contiene 9 dígitos");	
			}
		}catch(NumberFormatException e) 
		{
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("El número del teléfono contiene caracteres no válidos, vacio o nulo");
		}
		
		
		if(response.getEstado() == true)
		{
			response.setVeri_NroTelefono(nroTelefono);
		}
		return response;
	}
	
	public ResponseValidarCodigoVerificacion ValidarCodigoVerificacion(String codigo, String nroTelefono) 
	{
		ResponseValidarCodigoVerificacion response = new ResponseValidarCodigoVerificacion();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);	
		
		
		try 
		{
			Integer.parseInt(nroTelefono);
			
			if(nroTelefono.length()!=9)
			{
				response.setCodError(2);
				response.setEstado(false);
				response.setClie_Id(null);
				response.setMensaje("El número del teléfono no contiene 9 dígitos");	
			}
		}catch(NumberFormatException e) 
		{
			response.setCodError(1);
			response.setEstado(false);
			response.setClie_Id(null);
			response.setMensaje("El número del teléfono contiene caracteres no válidos, vacio o nulo");
		}
		
		
		return response;
	}
}
