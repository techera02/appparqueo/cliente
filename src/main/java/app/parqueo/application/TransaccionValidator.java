package app.parqueo.application;

import java.math.BigDecimal;

import app.parqueo.domain.model.ResponseError;
import app.parqueo.domain.model.Transaccion;

public class TransaccionValidator {

	public ResponseError recargarBilletera(Transaccion transaccion) {
		ResponseError response = new ResponseError();
		response.setEstado(true);
	
			if (transaccion.getTran_Monto().compareTo(BigDecimal.ZERO) <= 0) {
				response.setCodError(1);
				response.setEstado(false);
				response.setMensaje("El monto debe ser mayor a 0");
				
			}else if(transaccion.getClie_Id() == null) {
				response.setCodError(2);
				response.setEstado(false);
				response.setMensaje("El Id del cliente esta vacío o nulo");

			}else if(transaccion.getFrec_Id() == null) {
				response.setCodError(3);
				response.setEstado(false);
				response.setMensaje("El Id de la forma de recarga esta vacío o nulo");
			}

		
		return response;
		
	}
}
