package app.parqueo.application;

import app.parqueo.domain.model.ResponseError;
import app.parqueo.domain.model.Tarjeta;

public class TarjetaValidator {
	public ResponseError InsertarTarjeta(Tarjeta tarjeta)
	{
		
		ResponseError response = new ResponseError();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);	
		
		if(tarjeta.getClie_Id() == null || tarjeta.getClie_Id() ==0) {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("El Id del cliente vacío o nulo");	
		}else if (tarjeta.getTarj_Marca() == null || tarjeta.getTarj_Marca().length() == 0) {
			response.setEstado(false);
			response.setCodError(2);
			response.setMensaje("La marca de la tarjeta vacía o nulo");	
		}else if (tarjeta.getTarj_Numero() == null || tarjeta.getTarj_Numero().length() == 0) {
			response.setEstado(false);
			response.setCodError(3);
			response.setMensaje("El número de la tarjeta vacío o nulo");	
		}else if (tarjeta.getTarj_Token()  == null || tarjeta.getTarj_Numero().length() == 0) {
			response.setEstado(false);
			response.setCodError(4);
			response.setMensaje("El token de la tarjeta vacío o nulo");	
		}else if (tarjeta.getTarj_Numero().length() != 16) {
			response.setEstado(false);
			response.setCodError(5);
			response.setMensaje("El número de la tarjeta deber ser de 16 dígitos");	
		}else if (tarjeta.getTarj_Marca().length()>30) {
			response.setEstado(false);
			response.setCodError(6);
			response.setMensaje("El nombre de la marca no deber tener más de 30 dígitos");	
		}

		return response;
	}
}
