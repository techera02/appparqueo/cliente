package app.parqueo.application;

import org.springframework.stereotype.Service;

import java.util.List;

import app.parqueo.domain.model.Vehiculo;
import app.parqueo.domain.persistence_ports.VehiculoPersistence;

@Service
public class VehiculoService {
	VehiculoPersistence vehiculoPersistence;
	
	public VehiculoService(VehiculoPersistence vehiculoPersistence)
	{
		this.vehiculoPersistence = vehiculoPersistence;
	}
	
	public Integer insertarVehiculo(Vehiculo vehiculo) 
	{
        return this.vehiculoPersistence.insertarCliente(vehiculo);
    }

	public List<Vehiculo> listarVehiculosPorCliente(Integer clie_Id)
	{
		return this.vehiculoPersistence.listarVehiculosPorCliente(clie_Id);
	}

	public Integer eliminarVehiculoPorPlaca(String vehi_Placa) {
		return this.vehiculoPersistence.eliminarVehiculoPorPlaca(vehi_Placa);
	}
}
