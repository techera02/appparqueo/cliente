package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Parqueo;
import app.parqueo.domain.persistence_ports.ParqueoPersistence;

@Service
public class ParqueoService {
	ParqueoPersistence parqueoPersistence;
	
	public ParqueoService(ParqueoPersistence parqueoPersistence)
	{
		this.parqueoPersistence = parqueoPersistence;
	}
	
	public List<Parqueo> listarParqueosPorCliente (Integer clie_id, String fechaInicio, String fechaFin){
		return this.parqueoPersistence.ListarParqueosPorCliente(clie_id, fechaInicio, fechaFin);
	}
}
