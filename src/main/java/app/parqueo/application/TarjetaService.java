package app.parqueo.application;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Tarjeta;
import app.parqueo.domain.persistence_ports.TarjetaPersistence;

@Service
public class TarjetaService {
	TarjetaPersistence tarjetaPersistence;
	
	public TarjetaService(TarjetaPersistence tarjetaPersistence)
	{
		this.tarjetaPersistence = tarjetaPersistence;
	}
	
	public Integer insertarTarjeta(Tarjeta tarjeta) {
		return this.tarjetaPersistence.insertarTarjeta(tarjeta);
	}
}
