package app.parqueo.application;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Cliente;
import app.parqueo.domain.persistence_ports.ClientePersistence;

@Service
public class ClienteService 
{
	ClientePersistence clientePersistence;
	
	public ClienteService(ClientePersistence clientePersistence)
	{
		this.clientePersistence = clientePersistence;
	}
	
	public Integer insertarCliente(Cliente cliente) 
	{
        return this.clientePersistence.insertarCliente(cliente);
    }
	
	public Integer actualizarCliente(Cliente cliente) {
		return this.clientePersistence.actualizarCliente(cliente);
	}
	
	public Integer verificarCorreo(String correo, Integer idCliente) 
	{
        return this.clientePersistence.verificarCorreo(correo, idCliente);
    }
	
	public Cliente seleccionarCliente(Integer idCliente) {
		return this.clientePersistence.seleccionarCliente(idCliente);
	}
	
	public Integer validarClienteExiste(Integer idCliente) {
		return this.clientePersistence.validarClienteExiste(idCliente);
	}
	
}
