package app.parqueo.infrastructure.postgresql.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.ParqueoEntity;

public interface ParqueoRepository extends JpaRepository<ParqueoEntity, Integer>
{
	@Query(value = "SELECT * FROM \"UFN_ListarParqueosPorCliente\"(:clie_id,:fechaInicio,:fechaFin)", nativeQuery = true)
	List<ParqueoEntity> listarParqueosPorCliente(@Param("clie_id")Integer clie_id, @Param("fechaInicio")String fechaInicio, @Param("fechaFin")String fechaFin);
}
