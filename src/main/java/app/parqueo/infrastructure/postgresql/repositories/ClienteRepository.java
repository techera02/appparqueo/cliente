package app.parqueo.infrastructure.postgresql.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.ClienteEntity;

public interface ClienteRepository  extends JpaRepository<ClienteEntity, Integer>
{
	@Query(value = "SELECT \"UFN_ValidarCorreoCliente\"(:correo,:idcliente)", nativeQuery = true)
	Integer validarCorreoCliente(@Param("correo") String correo, @Param("idcliente") Integer idcliente);
	
	@Query(value = "SELECT \"UFN_InsertarCliente\"(:correo,:apellidopaterno,:apellidoMaterno,:nombres,:nroTelefono,:tokenfirebase)", nativeQuery = true)
	Integer insertarCliente(
			@Param("correo") String correo, @Param("apellidopaterno") String apellidopaterno,
			@Param("apellidoMaterno") String apellidoMaterno,@Param("nombres") String nombres,
			@Param("nroTelefono") String nroTelefono, @Param("tokenfirebase") String tokenfirebase
			);
	
	
	@Query(value = "CALL \"USP_Cliente_UPD\"(:correo, :apellidoPaterno,:apellidoMaterno,:nombres,:tdoc_id,:numeroDocumento,:idCliente)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarCliente(
			@Param("correo") String correo, @Param("apellidoPaterno") String apellidoPaterno,
			@Param("apellidoMaterno") String apellidoMaterno, @Param("nombres") String nombres,
			@Param("tdoc_id") Integer tdoc_id, @Param("numeroDocumento") String numeroDocumento,@Param("idCliente") Integer idCliente
			);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarCliente\"(:idCliente)", nativeQuery = true)
	ClienteEntity seleccionarCliente(@Param("idCliente") Integer idcliente);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarClienteExiste\"(:idCliente)", nativeQuery = true)
	Integer validarClienteExiste(@Param("idCliente") Integer idcliente);

}
