package app.parqueo.infrastructure.postgresql.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import app.parqueo.infrastructure.postgresql.entities.TransaccionEntity;
public interface TransaccionRepository extends CrudRepository<TransaccionEntity,Integer>, JpaSpecificationExecutor<TransaccionEntity>
{
	@Query(value = "CALL \"USP_RecargarBilletera_PROC\"(:clie_id,:monto,:frec_id,:tarj_id)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer recargarBilletera(@Param("clie_id")Integer clie_id,@Param("monto") BigDecimal monto, 
			@Param("frec_id")Integer frec_id,@Param("tarj_id") Integer tarj_id);
	
	@Query(value = "SELECT \"UNF_ValidarRecargaBilletera\"(:clie_id,:frec_id,:tarj_id)", nativeQuery = true)
	Integer ValidarRecargaBilletera(@Param("clie_id")Integer clie_id, @Param("frec_id")Integer frec_id,
			@Param("tarj_id") Integer tarj_id);
	
	@Query(nativeQuery = true, value = "SELECT * FROM \"UFN_ListarTransacciones\"(:clie_id,:fechaInicio,:fechaFin)")
	List<Object[]> listarTransacciones(@Param("clie_id")Integer clie_id, @Param("fechaInicio")String fechaInicio, @Param("fechaFin")String fechaFin);
	
	@Query(nativeQuery = true, value = "SELECT \"UNF_ValidarListarTransacciones\"(:clie_id)")
	Integer ValidarListarTransacciones(@Param("clie_id")Integer clie_id);

	@Query(nativeQuery = true, value = "SELECT \"UFN_SeleccionarSaldoBilletera\"(:clie_id)")
	Double seleccionarSaldoBilletera(@Param("clie_id")Integer clie_id);
}
