package app.parqueo.infrastructure.postgresql.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.TarjetaEntity;

public interface TarjetaRepository extends JpaRepository<TarjetaEntity, Integer>
{
	
	@Query(value = "CALL \"USP_Tarjeta_INS\"(:clie_id, :tarj_marca,:tarj_numero,:tarj_token)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer insertarTarjeta(@Param("clie_id")Integer clie_id, @Param("tarj_marca")String tarj_marca,
			@Param("tarj_numero")String tarj_numero, @Param("tarj_token")String tarj_token);
}
