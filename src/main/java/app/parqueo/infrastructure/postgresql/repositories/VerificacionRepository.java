package app.parqueo.infrastructure.postgresql.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.VerificacionEntity;

public interface VerificacionRepository  extends JpaRepository<VerificacionEntity, Integer>
{
	@Query(value = "CALL \"USP_Verificacion_INS\"(:nrotelefono)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer insertarVerificacion(@Param("nrotelefono") String nrotelefono);
	
	
	@Query(value = "SELECT \"UFN_ValidarCodigoVerificacionCliente\"(:codigo,:nrotelefono)", nativeQuery = true)
	Integer validarCodigoVerificacion(@Param("codigo") String codigo, @Param("nrotelefono") String nrotelefono);
	
	
	
}
