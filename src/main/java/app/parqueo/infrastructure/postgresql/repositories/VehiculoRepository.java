package app.parqueo.infrastructure.postgresql.repositories;

import javax.transaction.Transactional;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.VehiculoEntity;

public interface VehiculoRepository  extends JpaRepository<VehiculoEntity, Integer>
{
	@Query(value = "CALL \"USP_Vehiculo_INS\"(:clieId, :placa)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer insertarVehiculo(@Param("clieId") Integer clieId, @Param("placa") String placa);

	@Query(value = "SELECT *FROM \"UFN_ListarVehiculosPorCliente\"(:clie_Id)", nativeQuery = true)
	List<VehiculoEntity> listarVehiculosPorCliente(@Param("clie_Id") Integer clie_Id);

	@Query(value = "SELECT \"UFN_EliminarVehiculoPorPlaca\"(:vehi_placa)", nativeQuery = true)
	Integer eliminarVehiculoPorPlaca(@Param("vehi_placa") String vehi_placa);
}
