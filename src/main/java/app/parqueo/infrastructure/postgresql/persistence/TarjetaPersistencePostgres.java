package app.parqueo.infrastructure.postgresql.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Tarjeta;
import app.parqueo.domain.persistence_ports.TarjetaPersistence;
import app.parqueo.infrastructure.postgresql.repositories.TarjetaRepository;

@Repository("TarjetaPersistence")
public class TarjetaPersistencePostgres implements TarjetaPersistence
{

	private final TarjetaRepository tarjetaRepository;
	
    @Autowired
    public TarjetaPersistencePostgres(TarjetaRepository tarjetaRepository) 
    {
        this.tarjetaRepository = tarjetaRepository;
    }
    
	@Override
	public Integer insertarTarjeta(Tarjeta tarjeta) {
		return this.tarjetaRepository.insertarTarjeta(tarjeta.getClie_Id(),tarjeta.getTarj_Marca(),tarjeta.getTarj_Numero()
				,tarjeta.getTarj_Token());
	}
	
}
