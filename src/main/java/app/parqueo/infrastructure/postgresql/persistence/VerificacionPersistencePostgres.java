package app.parqueo.infrastructure.postgresql.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.persistence_ports.VerificacionPersistence;
import app.parqueo.infrastructure.postgresql.repositories.VerificacionRepository;

@Repository("VerificacionPersistence")
public class VerificacionPersistencePostgres implements VerificacionPersistence
{
	
	private final VerificacionRepository verificacionRepository;
	
    @Autowired
    public VerificacionPersistencePostgres(VerificacionRepository verificacionRepository) 
    {
        this.verificacionRepository = verificacionRepository;
    }
    
 
	@Override
	public Integer insertarVerificacion(String nroTelefono) 
	{
		return this.verificacionRepository.insertarVerificacion(nroTelefono);
	}


	@Override
	public Integer validarCodigoVerificacion(String codigo, String nroTelefono) {
		return this.verificacionRepository.validarCodigoVerificacion(codigo, nroTelefono);
	}
	
}
