package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Parqueo;
import app.parqueo.domain.persistence_ports.ParqueoPersistence;
import app.parqueo.infrastructure.postgresql.entities.ParqueoEntity;
import app.parqueo.infrastructure.postgresql.repositories.ParqueoRepository;

@Repository("ParqueoPersistence")
public class ParqueoPersistencePostgres implements ParqueoPersistence
{
	private final ParqueoRepository parqueoRepository;
	
    @Autowired
    public ParqueoPersistencePostgres(ParqueoRepository parqueoRepository) 
    {
        this.parqueoRepository = parqueoRepository;
    }
	@Override
	public List<Parqueo> ListarParqueosPorCliente(Integer clie_id, String fechaInicio, String fechaFin) {
		List<ParqueoEntity> parqueosEntity = this.parqueoRepository.listarParqueosPorCliente(clie_id, fechaInicio, fechaFin);
		List<Parqueo> parqueos = new ArrayList<Parqueo>();
		
		for (int i = 0; i < parqueosEntity.size(); i++) {
			parqueos.add(parqueosEntity.get(i).toParqueo());
		}
		
		return parqueos;
	}

}
