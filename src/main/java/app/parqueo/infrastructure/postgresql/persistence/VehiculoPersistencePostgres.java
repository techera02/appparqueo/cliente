package app.parqueo.infrastructure.postgresql.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


import app.parqueo.domain.model.Vehiculo;
import app.parqueo.domain.persistence_ports.VehiculoPersistence;
import app.parqueo.infrastructure.postgresql.repositories.VehiculoRepository;
import app.parqueo.infrastructure.postgresql.entities.VehiculoEntity;

@Repository("VehiculoPersistence")
public class VehiculoPersistencePostgres implements VehiculoPersistence
{

	private final VehiculoRepository vehiculoRepository;
	
    @Autowired
    public VehiculoPersistencePostgres(VehiculoRepository vehiculoRepository) 
    {
        this.vehiculoRepository = vehiculoRepository;
    }
	@Override
	public Integer insertarCliente(Vehiculo vehiculo) {
		return this.vehiculoRepository.insertarVehiculo(vehiculo.getClie_Id(), vehiculo.getVehi_Placa());
	}

    @Override
		public List<Vehiculo> listarVehiculosPorCliente(Integer clie_Id){
	        List<VehiculoEntity> vehiculosEntity = this.vehiculoRepository.listarVehiculosPorCliente(clie_Id);

			List<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
			
			for (int i = 0; i < vehiculosEntity.size(); i++) {
				vehiculos.add(vehiculosEntity.get(i).toVehiculo());
			}
	        return vehiculos;
	    }

	@Override
	public Integer eliminarVehiculoPorPlaca(String vehi_Placa) {
		return this.vehiculoRepository.eliminarVehiculoPorPlaca(vehi_Placa);
	}

}
