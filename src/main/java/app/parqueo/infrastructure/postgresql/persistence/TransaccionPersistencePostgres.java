package app.parqueo.infrastructure.postgresql.persistence;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.ResponseTransaccion;
import app.parqueo.domain.persistence_ports.TransaccionPersistence;
import app.parqueo.infrastructure.postgresql.repositories.TransaccionRepository;
@Repository("TransaccionPersistence")
public class TransaccionPersistencePostgres implements TransaccionPersistence
{

	private final TransaccionRepository transaccionRepository;
	
    @Autowired
    public TransaccionPersistencePostgres(TransaccionRepository transaccionRepository) 
    {
        this.transaccionRepository = transaccionRepository;
    }
    
	@Override
	public Integer recargarBilletera(Integer clie_id, BigDecimal monto, Integer frec_id, Integer tarj_id) {
		return this.transaccionRepository.recargarBilletera( clie_id,  monto,  frec_id,  tarj_id);
	}

	@Override
	public Integer validarRecargaBilletera(Integer clie_id, Integer frec_id, Integer tarj_id) {
		return this.transaccionRepository.ValidarRecargaBilletera(clie_id, frec_id, tarj_id);
	}

	@Override
	public List<ResponseTransaccion> listarTransacciones(Integer clie_id, String fechaInicio, String fechaFin) {
		List<ResponseTransaccion> listaTransacciones = new ArrayList<ResponseTransaccion>();
		
		List<Object[]> list = this.transaccionRepository.listarTransacciones(clie_id, fechaInicio, fechaFin);
		
		for(int i = 0; i<list.size();i++) {
			ResponseTransaccion transaccion = new ResponseTransaccion();
			
	        transaccion.setTran_Id(Integer.parseInt(list.get(i)[0].toString()));
	        transaccion.setTran_Tipo(list.get(i)[1].toString());
	        
	        if(list.get(i)[2] != null) {
		        BigDecimal saldoActual = new BigDecimal(Double.parseDouble(list.get(i)[2].toString()));
		        transaccion.setTran_SaldoActual(saldoActual.setScale(2, RoundingMode.HALF_UP));
	        }else {
	        	transaccion.setTran_SaldoActual(null);
	        }
	        
	        transaccion.setMuni_Nombre(list.get(i)[3].toString());
	        
	        if(list.get(i)[4] != null) {
		        BigDecimal monto = new BigDecimal(Double.parseDouble(list.get(i)[4].toString()));
		        transaccion.setTran_Monto(monto.setScale(2, RoundingMode.HALF_UP));
	        }else {
	        	transaccion.setTran_Monto(null);
	        }
	        
	        
	        transaccion.setTran_FechaCreacion(Timestamp.valueOf(list.get(i)[5].toString()));
	       	transaccion.setParq_FechaInicio(Timestamp.valueOf(list.get(i)[6].toString()));
	        transaccion.setParq_FechaFin(Timestamp.valueOf(list.get(i)[7].toString()));

	        listaTransacciones.add(transaccion);

		}

		return listaTransacciones;
		
	}

	@Override
	public Integer validarListarTransacciones(Integer clie_id) {
		return this.transaccionRepository.ValidarListarTransacciones(clie_id);
	}

	@Override
	public Double seleccionarSaldoBilletera(Integer clie_id) {
		return this.transaccionRepository.seleccionarSaldoBilletera(clie_id);
	}

}
