package app.parqueo.infrastructure.postgresql.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Cliente;
import app.parqueo.domain.persistence_ports.ClientePersistence;
import app.parqueo.infrastructure.postgresql.repositories.ClienteRepository;

@Repository("ClientePersistence")
public class ClientePersistencePostgres implements ClientePersistence
{
	private final ClienteRepository clienteRepository;
	
    @Autowired
    public ClientePersistencePostgres(ClienteRepository clienteRepository) 
    {
        this.clienteRepository = clienteRepository;
    }
    

	@Override
	public Integer insertarCliente(Cliente cliente) 
	{
		return this.clienteRepository.insertarCliente(cliente.getClie_Correo(), cliente.getClie_ApePaterno(), 
				cliente.getClie_ApeMaterno(), cliente.getClie_Nombres(),cliente.getClie_NroTelefono(),cliente.getClie_TokenFirebase());
	}


	@Override
	public Integer verificarCorreo(String correo, Integer idCliente) 
	{
		return this.clienteRepository.validarCorreoCliente(correo, idCliente);
	}


	@Override
	public Integer actualizarCliente(Cliente cliente) {
		return this.clienteRepository.actualizarCliente(cliente.getClie_Correo(), cliente.getClie_ApePaterno()
				, cliente.getClie_ApeMaterno(), cliente.getClie_Nombres(), cliente.getTdoc_Id(), 
				cliente.getClie_NroDocumento(), cliente.getClie_Id());
	}


	@Override
	public Cliente seleccionarCliente(Integer idCliente) {
		return this.clienteRepository.seleccionarCliente(idCliente).toCliente();
	}


	@Override
	public Integer validarClienteExiste(Integer idCliente) {
		return this.clienteRepository.validarClienteExiste(idCliente);
	}

}
