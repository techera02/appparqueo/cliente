package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Vehiculo;

@Entity
@Table(name="\"Vehiculo\"")
public class VehiculoEntity 
{
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer Vehi_Id;
	private Integer Clie_Id;
	private String Vehi_Placa;
	private boolean Vehi_Eliminado;
	private Timestamp Vehi_FechaCreacion;
	private Timestamp Vehi_FechaElimina;

	public Integer getVehi_Id() 
	{
		return Vehi_Id;
	}
	public void setVehi_Id(Integer vehi_Id)
	{
		Vehi_Id = vehi_Id;
	}
	public Integer getClie_Id() 
	{
		return Clie_Id;
	}
	public void setClie_Id(Integer clie_Id)
	{
		Clie_Id = clie_Id;
	}
	public String getVehi_Placa() 
	{
		return Vehi_Placa;
	}
	public void setVehi_Placa(String vehi_Placa)
	{
		Vehi_Placa = vehi_Placa;
	}

	public Timestamp getVehi_FechaElimina() 
	{
		return Vehi_FechaElimina;
	}
	public void setVehi_FechaElimina(Timestamp vehi_FechaElimina)
	{
		Vehi_FechaElimina = vehi_FechaElimina;
	}
	public Timestamp getVehi_FechaCreacion() {
		return Vehi_FechaCreacion;
	}
	public void setVehi_FechaCreacion(Timestamp vehi_FechaCreacion) {
		Vehi_FechaCreacion = vehi_FechaCreacion;
	}
	public boolean getVehi_Eliminado() {
		return Vehi_Eliminado;
	}
	public void setVehi_Eliminado(boolean vehi_Elimina) {
		Vehi_Eliminado = vehi_Elimina;
	}
	
	public Vehiculo toVehiculo() {
		Vehiculo vehiculo = new Vehiculo();
        BeanUtils.copyProperties(this, vehiculo);
        return vehiculo;
    }
	
}
