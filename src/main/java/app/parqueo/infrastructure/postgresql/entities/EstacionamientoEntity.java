package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"Estacionamiento\"")
public class EstacionamientoEntity 
{
	@Id
	private Integer Esta_Id;
	private Integer Zona_Id;
	private String Esta_Nombre;
	private Integer Esta_UsuarioCreacion;
	private Integer Esta_UsuarioEdicion;
	private Timestamp Esta_FechaCreacion;
	private Timestamp Esta_FechaEdicion;
	private String Esta_Geometry;
	private String Esta_Codigo;
	private boolean Esta_Disponible;
		
	public Integer getEsta_Id() {
		return Esta_Id;
	}
	public void setEsta_Id(Integer esta_Id) {
		Esta_Id = esta_Id;
	}
	public Integer getZona_Id() {
		return Zona_Id;
	}
	public void setZona_Id(Integer zona_Id) {
		Zona_Id = zona_Id;
	}
	public String getEsta_Nombre() {
		return Esta_Nombre;
	}
	public void setEsta_Nombre(String esta_Nombre) {
		Esta_Nombre = esta_Nombre;
	}
	public Integer getEsta_UsuarioCreacion() {
		return Esta_UsuarioCreacion;
	}
	public void setEsta_UsuarioCreacion(Integer esta_UsuarioCreacion) {
		Esta_UsuarioCreacion = esta_UsuarioCreacion;
	}
	public Integer getEsta_UsuarioEdicion() {
		return Esta_UsuarioEdicion;
	}
	public void setEsta_UsuarioEdicion(Integer esta_UsuarioEdicion) {
		Esta_UsuarioEdicion = esta_UsuarioEdicion;
	}
	public Timestamp getEsta_FechaCreacion() {
		return Esta_FechaCreacion;
	}
	public void setEsta_FechaCreacion(Timestamp esta_FechaCreacion) {
		Esta_FechaCreacion = esta_FechaCreacion;
	}
	public Timestamp getEsta_FechaEdicion() {
		return Esta_FechaEdicion;
	}
	public void setEsta_FechaEdicion(Timestamp esta_FechaEdicion) {
		Esta_FechaEdicion = esta_FechaEdicion;
	}
	public String getEsta_Geometry() {
		return Esta_Geometry;
	}
	public void setEsta_Geometry(String esta_Geometry) {
		Esta_Geometry = esta_Geometry;
	}
	public String getEsta_Codigo() {
		return Esta_Codigo;
	}
	public void setEsta_Codigo(String esta_Codigo) {
		Esta_Codigo = esta_Codigo;
	}
	public boolean isEsta_Disponible() {
		return Esta_Disponible;
	}
	public void setEsta_Disponible(boolean esta_Disponible) {
		Esta_Disponible = esta_Disponible;
	}
	
	
}
