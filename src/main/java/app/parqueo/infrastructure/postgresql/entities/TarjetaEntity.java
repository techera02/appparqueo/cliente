package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Entity
@Table(name = "\"Tarjeta\"")
public class TarjetaEntity {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer Tarj_Id;
	private Integer Clie_Id;
	private String Tarj_Marca;
	private String Tarj_Numero;
	private String Tarj_Token;
	private boolean Tarj_Eliminado;
	private Timestamp Tarj_FechaCreacion;
	private Timestamp Tarj_FechaElimina;
	
	public Integer getTarj_Id() {
		return Tarj_Id;
	}
	public void setTarj_Id(Integer tarj_Id) {
		Tarj_Id = tarj_Id;
	}
	public Integer getClie_Id() {
		return Clie_Id;
	}
	public void setClie_Id(Integer clie_Id) {
		Clie_Id = clie_Id;
	}
	public String getTarj_Marca() {
		return Tarj_Marca;
	}
	public void setTarj_Marca(String tarj_Marca) {
		Tarj_Marca = tarj_Marca;
	}
	public String getTarj_Numero() {
		return Tarj_Numero;
	}
	public void setTarj_Numero(String tarj_Numero) {
		Tarj_Numero = tarj_Numero;
	}
	public String getTarj_Token() {
		return Tarj_Token;
	}
	public void setTarj_Token(String tarj_Token) {
		Tarj_Token = tarj_Token;
	}
	public boolean isTarj_Eliminado() {
		return Tarj_Eliminado;
	}
	public void setTarj_Eliminado(boolean tarj_Eliminado) {
		Tarj_Eliminado = tarj_Eliminado;
	}
	public Timestamp getTarj_FechaCreacion() {
		return Tarj_FechaCreacion;
	}
	public void setTarj_FechaCreacion(Timestamp tarj_FechaCreacion) {
		Tarj_FechaCreacion = tarj_FechaCreacion;
	}
	public Timestamp getTarj_FechaElimina() {
		return Tarj_FechaElimina;
	}
	public void setTarj_FechaElimina(Timestamp tarj_FechaElimina) {
		Tarj_FechaElimina = tarj_FechaElimina;
	}

}