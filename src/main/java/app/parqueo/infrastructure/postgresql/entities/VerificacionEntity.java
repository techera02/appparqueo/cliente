package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"Verificacion\"")
public class VerificacionEntity  
{
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer Veri_Id;
	private String Veri_NroTelefono;
	private String Veri_Codigo;
	private Timestamp Veri_FechaExpiracion;
	
	public int getVeri_Id() 
	{
		return Veri_Id;
	}
	public void setVeri_Id(int veri_Id) 
	{
		Veri_Id = veri_Id;
	}
	public String getVeri_NroTelefono() 
	{
		return Veri_NroTelefono;
	}
	public void setVeri_NroTelefono(String veri_NroTelefono) 
	{
		Veri_NroTelefono = veri_NroTelefono;
	}
	public String getVeri_Codigo() 
	{
		return Veri_Codigo;
	}
	public void setVeri_Codigo(String veri_Codigo) 
	{
		Veri_Codigo = veri_Codigo;
	}
	public Timestamp getVeri_FechaExpiracion() 
	{
		return Veri_FechaExpiracion;
	}
	public void setVeri_FechaExpiracion(Timestamp veri_FechaExpiracion) 
	{
		Veri_FechaExpiracion = veri_FechaExpiracion;
	}
	

}
