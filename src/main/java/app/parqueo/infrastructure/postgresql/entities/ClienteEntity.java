package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Cliente;

@Entity
@Table(name="\"Cliente\"")
public class ClienteEntity 
{

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer Clie_Id;
	private String Clie_Nombres;
	private String Clie_ApePaterno;
	private String Clie_ApeMaterno;
	private Integer Tdoc_Id;
	private String Clie_NroDocumento;
	private String Clie_TokenFirebase;
	private Timestamp Clie_FechaCreacion;
	private Timestamp Clie_FechaEdicion;
	private String Clie_NroTelefono;
	private String Clie_Correo;
	
	public Integer getClie_Id() {
		return Clie_Id;
	}
	public void setClie_Id(Integer clie_Id) {
		Clie_Id = clie_Id;
	}
	public String getClie_Nombres() {
		return Clie_Nombres;
	}
	public void setClie_Nombres(String clie_Nombres) {
		Clie_Nombres = clie_Nombres;
	}
	public String getClie_ApePaterno() {
		return Clie_ApePaterno;
	}
	public void setClie_ApePaterno(String clie_ApePaterno) {
		Clie_ApePaterno = clie_ApePaterno;
	}
	public String getClie_ApeMaterno() {
		return Clie_ApeMaterno;
	}
	public void setClie_ApeMaterno(String clie_ApeMaterno) {
		Clie_ApeMaterno = clie_ApeMaterno;
	}
	public Integer getTdoc_Id() {
		return Tdoc_Id;
	}
	public void setTdoc_Id(Integer tdoc_Id) {
		Tdoc_Id = tdoc_Id;
	}
	public String getClie_NroDocumento() {
		return Clie_NroDocumento;
	}
	public void setClie_NroDocumento(String clie_NroDocumento) {
		Clie_NroDocumento = clie_NroDocumento;
	}
	public String getClie_TokenFirebase() {
		return Clie_TokenFirebase;
	}
	public void setClie_TokenFirebase(String clie_TokenFirebase) {
		Clie_TokenFirebase = clie_TokenFirebase;
	}
	public Timestamp getClie_FechaCreacion() {
		return Clie_FechaCreacion;
	}
	public void setClie_FechaCreacion(Timestamp clie_FechaCreacion) {
		Clie_FechaCreacion = clie_FechaCreacion;
	}
	public Timestamp getClie_FechaEdicion() {
		return Clie_FechaEdicion;
	}
	public void setClie_FechaEdicion(Timestamp clie_FechaEdicion) {
		Clie_FechaEdicion = clie_FechaEdicion;
	}
	public String getClie_NroTelefono() {
		return Clie_NroTelefono;
	}
	public void setClie_NroTelefono(String clie_NroTelefono) {
		Clie_NroTelefono = clie_NroTelefono;
	}
	public String getClie_Correo() {
		return Clie_Correo;
	}
	public void setClie_Correo(String clie_Correo) {
		Clie_Correo = clie_Correo;
	}
	
	public Cliente toCliente() {
		Cliente cliente = new Cliente();
        BeanUtils.copyProperties(this, cliente);
        return cliente;
    }
	
}
