package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.ClienteService;
import app.parqueo.application.ClienteValidator;
import app.parqueo.application.VehiculoService;
import app.parqueo.domain.model.Cliente;
import app.parqueo.domain.model.ResponseActualizarCliente;
import app.parqueo.domain.model.ResponseInsertarCliente;
import app.parqueo.domain.model.ResponseListarVehiculosPorCliente;
import app.parqueo.domain.model.ResponseSeleccionarCliente;

@RestController
@RequestMapping("/Cliente")
public class ClienteController 
{
	
	private final ClienteService clienteService;
	private final VehiculoService vehiculoService;

	
	@Autowired
    public ClienteController(ClienteService clienteService,VehiculoService vehiculoService) 
	{
        this.clienteService = clienteService;
        this.vehiculoService = vehiculoService;
    }
	
	@PostMapping("/InsertarCliente")
    public ResponseEntity<ResponseInsertarCliente> insertarCliente
    (@RequestBody Cliente cliente) 
	{  
		ResponseInsertarCliente response = new ResponseInsertarCliente();
		ClienteValidator clienteValidator = new ClienteValidator();

		response = clienteValidator.InsertarCliente(cliente);
		
		if(response.getEstado() == true) {
			
			try {
				Integer temp1 = this.clienteService.verificarCorreo(cliente.getClie_Correo(), 0);
				
				if(temp1 == 0) {
					Integer indice = this.clienteService.insertarCliente(cliente);
					cliente.setClie_Id(indice);
					
					for(int i=0; i < cliente.getVehiculos().size(); i++) {
						cliente.getVehiculos().get(i).setClie_Id(indice);
						this.vehiculoService.insertarVehiculo(cliente.getVehiculos().get(i));
					}
					response.setClie_Id(indice);
				}else {
					response.setCodError(2);
					response.setMensaje("El correo ya existe");
					response.setEstado(false);
				}
			}catch(Exception e) {
				response.setCodError(0);
				response.setMensaje("Error no controlado");
				response.setEstado(false);
			}
			
		}
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/ActualizarCliente")
	public ResponseEntity<ResponseActualizarCliente> actualizarCliente
	(@RequestBody Cliente cliente) 
	{ 
		ResponseActualizarCliente response = new ResponseActualizarCliente();
		response.setResultado("No se han actualizado los datos del cliente");
		response.setEstado(true);

		try {
			Integer temp1 = this.clienteService.verificarCorreo(cliente.getClie_Correo(), cliente.getClie_Id());

			if(temp1 == 1) {
				this.clienteService.actualizarCliente(cliente);

				if(cliente.getVehiculos() != null){
					for(int i=0; i < cliente.getVehiculos().size(); i++) {
						cliente.getVehiculos().get(i).setClie_Id(cliente.getClie_Id());
						this.vehiculoService.insertarVehiculo(cliente.getVehiculos().get(i));
					}
				}

				response.setResultado("Se han actualizado los datos del cliente");

			}else {
				response.setCodError(1);
				response.setMensaje("El correo ya esta registrado");
				response.setEstado(false);
			}
		}catch(Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
			
		}
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/SeleccionarCliente/{idCliente}")
	public ResponseEntity<ResponseSeleccionarCliente> seleccionarCliente(@PathVariable("idCliente")Integer idCliente) {
		ResponseSeleccionarCliente response = new ResponseSeleccionarCliente();
		response.setEstado(true);
		
		try {
			Integer temp = this.clienteService.validarClienteExiste(idCliente);
			
			if(temp == 1) {
				response.setCliente(this.clienteService.seleccionarCliente(idCliente));
				response.getCliente().setVehiculos(this.vehiculoService.listarVehiculosPorCliente(idCliente));
			}else {
				response.setCodError(1);
				response.setMensaje("Cliente no registrado");
				response.setEstado(false);
			}
			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/ListarVehiculosPorCliente/{idCliente}")
	public ResponseEntity<ResponseListarVehiculosPorCliente> listarVehiculosPorCliente(@PathVariable("idCliente")Integer idCliente) {
		ResponseListarVehiculosPorCliente response = new ResponseListarVehiculosPorCliente();
		response.setEstado(true);
		
		try {
			Integer temp = this.clienteService.validarClienteExiste(idCliente);
			
			if(temp == 1) {
				response.setVehiculos(vehiculoService.listarVehiculosPorCliente(idCliente));

			}else {
				response.setCodError(1);
				response.setMensaje("Cliente no registrado");
				response.setEstado(false);
			}
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	
}
