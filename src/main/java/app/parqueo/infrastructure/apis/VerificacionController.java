package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.VerificacionService;
import app.parqueo.application.VerificacionValidator;
import app.parqueo.domain.model.ResponseValidarCodigoVerificacion;
import app.parqueo.domain.model.ResponseValidarNumeroTelefono;


@RestController
@RequestMapping("/Verificacion")
public class VerificacionController 
{
	private final VerificacionService verificacionService;
	
	@Autowired
    public VerificacionController(VerificacionService verificacionService) 
	{
        this.verificacionService = verificacionService;
    }
	
	@GetMapping("/ValidarNumeroTelefonoCliente/{NroTelefono}")
    public ResponseEntity<ResponseValidarNumeroTelefono> insertarVerificacion
    (@PathVariable("NroTelefono")String NroTelefono) 
	{  
		ResponseValidarNumeroTelefono response = new ResponseValidarNumeroTelefono();
		VerificacionValidator verificacionValidator = new VerificacionValidator();
		
		response = verificacionValidator.ValidarNumeroTelefonoCliente(NroTelefono);

		if(response.getEstado() == true) {
			try {
				verificacionService.insertarVerificacion(NroTelefono);
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje("Error no controlado");
			}
			
		}
		
		return ResponseEntity.ok(response);
    }
	
	@GetMapping("/ValidarCodigoVerificacionCliente/{codigo}/{nroTelefono}")
    public ResponseEntity<ResponseValidarCodigoVerificacion> validarCodigoVerificacion
    (@PathVariable("codigo")String codigo, @PathVariable("nroTelefono")String nroTelefono) 
	{  
		ResponseValidarCodigoVerificacion response = new ResponseValidarCodigoVerificacion();
		try {
			Integer temp = verificacionService.validarCodigoVerificacion(codigo,nroTelefono);
			
			if(temp<0) {
				response.setEstado(false);
				response.setClie_Id(null);
				response.setCodError(temp * -1);
				
				if(temp == -1) {
					response.setMensaje("El número del teléfono no existe en la tabla verificación");
				}else if(temp == -2) {
					response.setMensaje("El código de verificación es incorrecto");
				}else if(temp == -3) {
					response.setMensaje("El código de verificación expiró");
				}
				
			}else {
				response.setEstado(true);
				response.setCodError(null);
				response.setClie_Id(temp);
			}

		}catch(Exception e) {
			response.setEstado(false);
			response.setClie_Id(null);
			response.setCodError(0);
		}
	
		
		return ResponseEntity.ok(response);
    }
}
