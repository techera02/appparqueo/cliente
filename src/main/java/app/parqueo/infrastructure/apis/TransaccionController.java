package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.ClienteService;
import app.parqueo.application.TransaccionService;
import app.parqueo.application.TransaccionValidator;
import app.parqueo.domain.model.ResponseError;
import app.parqueo.domain.model.ResponseListarTransacciones;
import app.parqueo.domain.model.Transaccion;

@RestController
@RequestMapping("/Transaccion")
public class TransaccionController 
{
	private final TransaccionService transaccionService;
	private final ClienteService clienteService;
	
	@Autowired
    public TransaccionController(TransaccionService transaccionService, ClienteService clienteService) 
	{
        this.transaccionService = transaccionService;
        this.clienteService = clienteService;
    }
	

	@PostMapping("/RecargarBilletera")
    public ResponseEntity<ResponseError> recargarBilletera
    (@RequestBody Transaccion transaccion) 
	{  
		ResponseError response = new ResponseError();
		TransaccionValidator transaccionValidator= new TransaccionValidator();
		
		response = transaccionValidator.recargarBilletera(transaccion);
		
		if(response.getEstado() == true) {
			
			if (transaccion.getTarj_Id() == null) {
				transaccion.setTarj_Id(0);
			}
			
			Integer temp = this.transaccionService.validarRecargaBilletera(transaccion.getClie_Id(), transaccion.getFrec_Id(), 
					transaccion.getTarj_Id());
						
			if (temp == 0) {
			
				try {
					this.transaccionService.recargarBilletera(transaccion.getClie_Id()
						,transaccion.getTran_Monto(), transaccion.getFrec_Id(), transaccion.getTarj_Id());
				}catch(Exception e) {
					response.setCodError(0);
					response.setMensaje("Error no controlado");
					response.setEstado(false);
				}
			}else {
				response.setCodError(temp);
				response.setEstado(false);
				if(temp == 4) {
					response.setMensaje("El cliente no esta registrado");
				}else if (temp == 5) {
					response.setMensaje("La forma de pago no existe");
				}else if (temp == 6) {
					response.setMensaje("La tarjeta no esta registrado");
				}
			}
		}
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/ListarTransacciones/{clie_id}/{fechaInicio}/{fechaFin}")
    public ResponseEntity<ResponseListarTransacciones> listarTransacciones
    (@PathVariable("clie_id") Integer clie_id, @PathVariable("fechaInicio") String fechaInicio, @PathVariable("fechaFin") String fechaFin ) 
	{
		
		ResponseListarTransacciones response = new ResponseListarTransacciones();
		response.setEstado(true);

		Integer temp = this.clienteService.validarClienteExiste(clie_id);
		
		if (temp == 1) {
			try {
				Integer temp2 = this.transaccionService.validarListarTransacciones(clie_id);
				
				if (temp2 == 1) {
					try {
						response.setTransacciones(this.transaccionService.listarTransacciones(clie_id,fechaInicio, fechaFin ));
					}catch(Exception e) {
						response.setCodError(3);
						response.setEstado(false);
						response.setMensaje("No se pudo listar las transacciones del cliente");
					}
					
					try {
						response.setBille_Saldo(this.transaccionService.seleccionarSaldoBilletera(clie_id));
					}catch(Exception e) {
						response.setCodError(4);
						response.setEstado(false);
						response.setMensaje("No se pudo obtener el saldo actual de la billetera ");
					}
				}else {
					response.setCodError(2);
					response.setEstado(false);
					response.setMensaje("El cliente tiene 0 transaccciones");
				}
			
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje("Error no controlado");
			}
			
		}else {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Cliente no registrado");
		}
		
		return ResponseEntity.ok(response);
	}

}
