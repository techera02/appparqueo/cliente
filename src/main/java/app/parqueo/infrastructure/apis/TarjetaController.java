package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.TarjetaService;
import app.parqueo.application.TarjetaValidator;
import app.parqueo.domain.model.ResponseError;
import app.parqueo.domain.model.Tarjeta;

@RestController
@RequestMapping("/Tarjeta")
public class TarjetaController {
	private final TarjetaService tarjetaService;
	
	@Autowired
    public TarjetaController(TarjetaService tarjetaService) 
	{
        this.tarjetaService = tarjetaService;
    }
	
	@PostMapping("/InsertarTarjeta")
    public ResponseEntity<ResponseError> insertarTarjeta
    (@RequestBody Tarjeta tarjeta) 
	{  
		ResponseError response = new ResponseError();
		TarjetaValidator tarjetaValidator = new TarjetaValidator();
		
		response  = tarjetaValidator.InsertarTarjeta(tarjeta);
		
		if(response.getEstado() == true) {
			try {
				this.tarjetaService.insertarTarjeta(tarjeta);
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje("Error no controlado");
			}
		}
		return ResponseEntity.ok(response);
	}
}
