package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.VehiculoService;
import app.parqueo.domain.model.ResponseEliminarVehiculoPorPlaca;


@RestController
@RequestMapping("/Vehiculo")
public class VehiculoController {
	
	private final VehiculoService vehiculoService;
	
	@Autowired
    public VehiculoController(VehiculoService vehiculoService) 
	{
        this.vehiculoService = vehiculoService;
    }
	
	@DeleteMapping("/EliminarVehiculoPorPlaca/{vehi_Placa}")
    public ResponseEntity<ResponseEliminarVehiculoPorPlaca> eliminarVehiculoPorPlaca
    (@PathVariable("vehi_Placa")String vehi_Placa) 
	{  
		ResponseEliminarVehiculoPorPlaca response = new ResponseEliminarVehiculoPorPlaca();
		response.setEstado(true);
		
		
		try {
			Integer temp = this.vehiculoService.eliminarVehiculoPorPlaca(vehi_Placa);
			
			if(temp==1) {
				response.setResultado(vehi_Placa);
			}else if(temp==0) {
				response.setCodError(1);
				response.setMensaje("Placa no registrada");
				response.setEstado(false);
			}
		}catch (Exception e){
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		
		return ResponseEntity.ok(response);
    }
	
}

