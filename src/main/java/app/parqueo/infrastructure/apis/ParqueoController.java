package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.ClienteService;
import app.parqueo.application.ParqueoService;
import app.parqueo.domain.model.ResponseListarParqueosPorCliente;

@RestController
@RequestMapping("/Parqueo")
public class ParqueoController 
{
	private final ClienteService clienteService;
	private final ParqueoService parqueoService;
	
	@Autowired
    public ParqueoController(ParqueoService parqueoService, ClienteService clienteService) 
	{
        this.parqueoService = parqueoService;
        this.clienteService = clienteService;
    }
	
	@GetMapping("/ListarParqueosPorCliente/{clie_id}/{fechaInicio}/{fechaFin}")
    public ResponseEntity<ResponseListarParqueosPorCliente> listarParqueosPorCliente
    (@PathVariable("clie_id")Integer clie_id, @PathVariable("fechaInicio") String fechaInicio, @PathVariable("fechaFin") String fechaFin ) 
	{  
		ResponseListarParqueosPorCliente response = new ResponseListarParqueosPorCliente();
		response.setEstado(true);
		
		Integer temp = this.clienteService.validarClienteExiste(clie_id);
		
		if(temp == 1) {
			try {
				response.setParqueos(this.parqueoService.listarParqueosPorCliente(clie_id, fechaInicio, fechaFin));
			}catch(Exception e) {
				response.setCodError(0);
				response.setMensaje("Error no controlado");
				response.setEstado(false);
			}
			
		}else {
			response.setCodError(1);
			response.setMensaje("Cliente no registrado");
			response.setEstado(false);
		}		
	
		
		return ResponseEntity.ok(response);
    }
	
}
