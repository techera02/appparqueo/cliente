package app.parqueo.domain.persistence_ports;

import app.parqueo.domain.model.Cliente;

public interface ClientePersistence 
{
	Integer insertarCliente(Cliente cliente);
	
	Integer actualizarCliente(Cliente cliente);
	
	Integer verificarCorreo(String correo, Integer idCliente);
	
	Cliente seleccionarCliente(Integer idCliente);
	
	Integer validarClienteExiste(Integer idCliente);
}
