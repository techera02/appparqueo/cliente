package app.parqueo.domain.persistence_ports;

import app.parqueo.domain.model.Tarjeta;

public interface TarjetaPersistence 
{
	Integer insertarTarjeta(Tarjeta tarjeta);
}
