package app.parqueo.domain.persistence_ports;

public interface VerificacionPersistence 
{
	Integer insertarVerificacion(String nroTelefono);
	
	Integer validarCodigoVerificacion(String codigo, String nroTelefono);
}
