package app.parqueo.domain.persistence_ports;

import java.math.BigDecimal;
import java.util.List;

import app.parqueo.domain.model.ResponseTransaccion;

public interface TransaccionPersistence {
	Integer recargarBilletera(Integer clie_id, BigDecimal monto, Integer frec_id, Integer tarj_id);
	
	Integer validarRecargaBilletera(Integer clie_id, Integer frec_id, Integer tarj_id);
	
	List<ResponseTransaccion> listarTransacciones(Integer clie_id, String fechaInicio, String fechaFin);
	
	Integer validarListarTransacciones(Integer clie_id);

	Double seleccionarSaldoBilletera(Integer clie_id);
	
}
