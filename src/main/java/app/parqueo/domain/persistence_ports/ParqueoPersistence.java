package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.Parqueo;

public interface ParqueoPersistence 
{
	List<Parqueo> ListarParqueosPorCliente(Integer clie_id, String fechaInicio, String fechaFin);
	
}
