package app.parqueo.domain.persistence_ports;

import app.parqueo.domain.model.Vehiculo;
import java.util.List;


public interface VehiculoPersistence {
	Integer insertarCliente(Vehiculo vehiculo);

	List<Vehiculo> listarVehiculosPorCliente(Integer clie_Id);

	Integer eliminarVehiculoPorPlaca(String vehi_Placa);
}
