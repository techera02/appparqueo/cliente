package app.parqueo.domain.model;

public class ResponseSeleccionarCliente extends ResponseError{
	private Cliente cliente;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
