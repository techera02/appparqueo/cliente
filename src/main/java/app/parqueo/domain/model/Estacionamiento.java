package app.parqueo.domain.model;

import java.sql.Timestamp;

public class Estacionamiento 
{
	private Integer esta_Id;
	private Integer zona_Id;
	private String esta_Nombre;
	private Integer esta_UsuarioCreacion;
	private Integer esta_UsuarioEdicion;
	private Timestamp esta_FechaCreacion;
	private Timestamp esta_FechaEdicion;
	private String esta_Geometry;
	private String esta_Codigo;
	private boolean esta_Disponible;
	public Integer getEsta_Id() {
		return esta_Id;
	}
	public void setEsta_Id(Integer esta_Id) {
		this.esta_Id = esta_Id;
	}
	public Integer getZona_Id() {
		return zona_Id;
	}
	public void setZona_Id(Integer zona_Id) {
		this.zona_Id = zona_Id;
	}
	public String getEsta_Nombre() {
		return esta_Nombre;
	}
	public void setEsta_Nombre(String esta_Nombre) {
		this.esta_Nombre = esta_Nombre;
	}
	public Integer getEsta_UsuarioCreacion() {
		return esta_UsuarioCreacion;
	}
	public void setEsta_UsuarioCreacion(Integer esta_UsuarioCreacion) {
		this.esta_UsuarioCreacion = esta_UsuarioCreacion;
	}
	public Integer getEsta_UsuarioEdicion() {
		return esta_UsuarioEdicion;
	}
	public void setEsta_UsuarioEdicion(Integer esta_UsuarioEdicion) {
		this.esta_UsuarioEdicion = esta_UsuarioEdicion;
	}
	public Timestamp getEsta_FechaCreacion() {
		return esta_FechaCreacion;
	}
	public void setEsta_FechaCreacion(Timestamp esta_FechaCreacion) {
		this.esta_FechaCreacion = esta_FechaCreacion;
	}
	public Timestamp getEsta_FechaEdicion() {
		return esta_FechaEdicion;
	}
	public void setEsta_FechaEdicion(Timestamp esta_FechaEdicion) {
		this.esta_FechaEdicion = esta_FechaEdicion;
	}
	public String getEsta_Geometry() {
		return esta_Geometry;
	}
	public void setEsta_Geometry(String esta_Geometry) {
		this.esta_Geometry = esta_Geometry;
	}
	public String getEsta_Codigo() {
		return esta_Codigo;
	}
	public void setEsta_Codigo(String esta_Codigo) {
		this.esta_Codigo = esta_Codigo;
	}
	public boolean isEsta_Disponible() {
		return esta_Disponible;
	}
	public void setEsta_Disponible(boolean esta_Disponible) {
		this.esta_Disponible = esta_Disponible;
	}
	
	
}
