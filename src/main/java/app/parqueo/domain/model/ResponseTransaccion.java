package app.parqueo.domain.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class ResponseTransaccion {
	private long tran_Id;
	private String tran_Tipo;
	private BigDecimal tran_SaldoActual;	
	private String muni_Nombre;
	private BigDecimal tran_Monto;
	private Timestamp tran_FechaCreacion;
	private Timestamp parq_FechaInicio;
	private Timestamp parq_FechaFin;
	public long getTran_Id() {
		return tran_Id;
	}
	public void setTran_Id(long tran_Id) {
		this.tran_Id = tran_Id;
	}
	public String getTran_Tipo() {
		return tran_Tipo;
	}
	public void setTran_Tipo(String tran_Tipo) {
		this.tran_Tipo = tran_Tipo;
	}
	public BigDecimal getTran_SaldoActual() {
		return tran_SaldoActual;
	}
	public void setTran_SaldoActual(BigDecimal tran_SaldoActual) {
		this.tran_SaldoActual = tran_SaldoActual;
	}
	public String getMuni_Nombre() {
		return muni_Nombre;
	}
	public void setMuni_Nombre(String muni_Nombre) {
		this.muni_Nombre = muni_Nombre;
	}
	public BigDecimal getTran_Monto() {
		return tran_Monto;
	}
	public void setTran_Monto(BigDecimal tran_Monto) {
		this.tran_Monto = tran_Monto;
	}
	public Timestamp getTran_FechaCreacion() {
		return tran_FechaCreacion;
	}
	public void setTran_FechaCreacion(Timestamp timestamp) {
		this.tran_FechaCreacion = timestamp;
	}
	public Timestamp getParq_FechaInicio() {
		return parq_FechaInicio;
	}
	public void setParq_FechaInicio(Timestamp parq_FechaInicio) {
		this.parq_FechaInicio = parq_FechaInicio;
	}
	public Timestamp getParq_FechaFin() {
		return parq_FechaFin;
	}
	public void setParq_FechaFin(Timestamp parq_FechaFin) {
		this.parq_FechaFin = parq_FechaFin;
	}

}
