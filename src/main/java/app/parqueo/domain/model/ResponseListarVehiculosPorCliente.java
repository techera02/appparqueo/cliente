package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarVehiculosPorCliente extends ResponseError{
    private List<Vehiculo> vehiculos;

	public List<Vehiculo> getVehiculos() {
		return vehiculos;
	}
	public void setVehiculos(List<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}	
}

