package app.parqueo.domain.model;

import java.sql.Timestamp;

public class Municipio 
{
	private Integer muni_Id;
	private String muni_Nombre;
	private boolean muni_Activo;
	private Integer muni_UsuarioCreacion;
	private Integer muni_UsuarioEdicion;
	private Timestamp muni_FechaCreacion;
	private Timestamp muni_FechaEdicion;
	public Integer getMuni_Id() {
		return muni_Id;
	}
	public void setMuni_Id(Integer muni_Id) {
		this.muni_Id = muni_Id;
	}
	public String getMuni_Nombre() {
		return muni_Nombre;
	}
	public void setMuni_Nombre(String muni_Nombre) {
		this.muni_Nombre = muni_Nombre;
	}
	public boolean isMuni_Activo() {
		return muni_Activo;
	}
	public void setMuni_Activo(boolean muni_Activo) {
		this.muni_Activo = muni_Activo;
	}
	public Integer getMuni_UsuarioCreacion() {
		return muni_UsuarioCreacion;
	}
	public void setMuni_UsuarioCreacion(Integer muni_UsuarioCreacion) {
		this.muni_UsuarioCreacion = muni_UsuarioCreacion;
	}
	public Integer getMuni_UsuarioEdicion() {
		return muni_UsuarioEdicion;
	}
	public void setMuni_UsuarioEdicion(Integer muni_UsuarioEdicion) {
		this.muni_UsuarioEdicion = muni_UsuarioEdicion;
	}
	public Timestamp getMuni_FechaCreacion() {
		return muni_FechaCreacion;
	}
	public void setMuni_FechaCreacion(Timestamp muni_FechaCreacion) {
		this.muni_FechaCreacion = muni_FechaCreacion;
	}
	public Timestamp getMuni_FechaEdicion() {
		return muni_FechaEdicion;
	}
	public void setMuni_FechaEdicion(Timestamp muni_FechaEdicion) {
		this.muni_FechaEdicion = muni_FechaEdicion;
	}

}
