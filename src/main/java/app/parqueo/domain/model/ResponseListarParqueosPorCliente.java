package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarParqueosPorCliente extends ResponseError
{
	private List<Parqueo> parqueos;

	public List<Parqueo> getParqueos() {
		return parqueos;
	}

	public void setParqueos(List<Parqueo> parqueos) {
		this.parqueos = parqueos;
	}
	
}
