package app.parqueo.domain.model;

import java.sql.Timestamp;

public class Verificacion {
	private Integer veri_Id;
	private String veri_NroTelefono;
	private String veri_Codigo;
	private Timestamp veri_FechaExpiracion;
	
	public Verificacion() {
	
	}

	public Integer getVeri_Id() {
		return veri_Id;
	}

	public void setVeri_Id(Integer veri_Id) {
		this.veri_Id = veri_Id;
	}

	public String getVeri_NroTelefono() {
		return veri_NroTelefono;
	}

	public void setVeri_NroTelefono(String veri_NroTelefono) {
		this.veri_NroTelefono = veri_NroTelefono;
	}

	public String getVeri_Codigo() {
		return veri_Codigo;
	}

	public void setVeri_Codigo(String veri_Codigo) {
		this.veri_Codigo = veri_Codigo;
	}

	public Timestamp getVeri_FechaExpiracion() {
		return veri_FechaExpiracion;
	}

	public void setVeri_FechaExpiracion(Timestamp veri_FechaExpiracion) {
		this.veri_FechaExpiracion = veri_FechaExpiracion;
	}
	
	
}
