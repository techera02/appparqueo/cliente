package app.parqueo.domain.model;

public class ResponseValidarCodigoVerificacion extends ResponseError
{
	private Integer clie_Id;

	public Integer getClie_Id() {
		return this.clie_Id;
	}

	public void setClie_Id(Integer veri_Id) {
		this.clie_Id = veri_Id;
	}
	
}
