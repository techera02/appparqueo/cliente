package app.parqueo.domain.model;

import java.sql.Timestamp;

public class Vehiculo
{

	private Integer vehi_Id;
	private Integer clie_Id;
	private String vehi_Placa;
	private boolean vehi_Eliminado;
	private Timestamp vechi_FechaCreacion;
	private Timestamp vehi_FechaElimina;
	
	public Vehiculo() {
		
	}

	public Integer getVehi_Id() {
		return vehi_Id;
	}

	public void setVehi_Id(Integer vehi_Id) {
		this.vehi_Id = vehi_Id;
	}

	public Integer getClie_Id() {
		return clie_Id;
	}

	public void setClie_Id(Integer clie_Id) {
		this.clie_Id = clie_Id;
	}

	public String getVehi_Placa() {
		return vehi_Placa;
	}

	public void setVehi_Placa(String vehi_Placa) {
		this.vehi_Placa = vehi_Placa;
	}

	public boolean isVehi_Eliminado() {
		return vehi_Eliminado;
	}

	public void setVehi_Eliminado(boolean vehi_Eliminado) {
		this.vehi_Eliminado = vehi_Eliminado;
	}

	public Timestamp getVechi_FechaCreacion() {
		return vechi_FechaCreacion;
	}

	public void setVechi_FechaCreacion(Timestamp vechi_FechaCreacion) {
		this.vechi_FechaCreacion = vechi_FechaCreacion;
	}

	public Timestamp getVehi_FechaElimina() {
		return vehi_FechaElimina;
	}

	public void setVehi_FechaElimina(Timestamp vehi_FechaElimina) {
		this.vehi_FechaElimina = vehi_FechaElimina;
	}

	
	
}
