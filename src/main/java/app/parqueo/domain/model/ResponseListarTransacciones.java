package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarTransacciones extends ResponseError{
	private Double bille_Saldo;

	private List<ResponseTransaccion> transacciones;

	public List<ResponseTransaccion> getTransacciones() {
		return transacciones;
	}

	public void setTransacciones(List<ResponseTransaccion> transacciones) {
		this.transacciones = transacciones;
	}

	public Double getBille_Saldo() {
		return bille_Saldo;
	}

	public void setBille_Saldo(Double bille_Saldo) {
		this.bille_Saldo = bille_Saldo;
	}
}
