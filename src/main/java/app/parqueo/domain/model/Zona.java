package app.parqueo.domain.model;

import java.sql.Timestamp;

public class Zona 
{
	private Integer zona_Id;
	private String zona_Descripcion;
	private Integer muni_Id;
	private boolean zona_Activo;
	private Integer zona_UsuarioCreacion;
	private Integer zona_UsuarioEdicion;
	private Timestamp zona_FechaCreacion;
	private Timestamp zona_FechaEdicion;
	private String zona_Geometry;
	public Integer getZona_Id() {
		return zona_Id;
	}
	public void setZona_Id(Integer zona_Id) {
		this.zona_Id = zona_Id;
	}
	public String getZona_Descripcion() {
		return zona_Descripcion;
	}
	public void setZona_Descripcion(String zona_Descripcion) {
		this.zona_Descripcion = zona_Descripcion;
	}
	public Integer getMuni_Id() {
		return muni_Id;
	}
	public void setMuni_Id(Integer muni_Id) {
		this.muni_Id = muni_Id;
	}
	public boolean isZona_Activo() {
		return zona_Activo;
	}
	public void setZona_Activo(boolean zona_Activo) {
		this.zona_Activo = zona_Activo;
	}
	public Integer getZona_UsuarioCreacion() {
		return zona_UsuarioCreacion;
	}
	public void setZona_UsuarioCreacion(Integer zona_UsuarioCreacion) {
		this.zona_UsuarioCreacion = zona_UsuarioCreacion;
	}
	public Integer getZona_UsuarioEdicion() {
		return zona_UsuarioEdicion;
	}
	public void setZona_UsuarioEdicion(Integer zona_UsuarioEdicion) {
		this.zona_UsuarioEdicion = zona_UsuarioEdicion;
	}
	public Timestamp getZona_FechaCreacion() {
		return zona_FechaCreacion;
	}
	public void setZona_FechaCreacion(Timestamp zona_FechaCreacion) {
		this.zona_FechaCreacion = zona_FechaCreacion;
	}
	public Timestamp getZona_FechaEdicion() {
		return zona_FechaEdicion;
	}
	public void setZona_FechaEdicion(Timestamp zona_FechaEdicion) {
		this.zona_FechaEdicion = zona_FechaEdicion;
	}
	public String getZona_Geometry() {
		return zona_Geometry;
	}
	public void setZona_Geometry(String zona_Geometry) {
		this.zona_Geometry = zona_Geometry;
	}
	
	
	
}
