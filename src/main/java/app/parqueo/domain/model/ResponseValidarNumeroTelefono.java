package app.parqueo.domain.model;

public class ResponseValidarNumeroTelefono extends ResponseError
{
	private String veri_NroTelefono;
	
	public String getVeri_NroTelefono() {
		return this.veri_NroTelefono;
	}

	public void setVeri_NroTelefono(String veri_NroTelefono) {
		this.veri_NroTelefono = veri_NroTelefono;
	}
	
	
}
