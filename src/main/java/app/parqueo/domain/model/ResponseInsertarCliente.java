package app.parqueo.domain.model;

public class ResponseInsertarCliente extends ResponseError
{
	private Integer clie_Id;

	public Integer getClie_Id() {
		return this.clie_Id;
	}

	public void setClie_Id(Integer clie_Id) {
		this.clie_Id = clie_Id;
	}
	
}
