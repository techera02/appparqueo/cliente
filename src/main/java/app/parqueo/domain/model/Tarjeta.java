package app.parqueo.domain.model;

import java.sql.Timestamp;

public class Tarjeta {
	private Integer tarj_Id;
	private Integer clie_Id;
	private String tarj_Marca;
	private String tarj_Numero;
	private String tarj_Token;
	private boolean tarj_Eliminado;	
	private Timestamp tarj_FechaCreacion;
	private Timestamp tarj_FechaElimina;
	
	public Integer getTarj_Id() {
		return tarj_Id;
	}
	public void setTarj_Id(Integer tarj_Id) {
		this.tarj_Id = tarj_Id;
	}
	public Integer getClie_Id() {
		return clie_Id;
	}
	public void setClie_Id(Integer clie_Id) {
		this.clie_Id = clie_Id;
	}
	public String getTarj_Marca() {
		return tarj_Marca;
	}
	public void setTarj_Marca(String tarj_Marca) {
		this.tarj_Marca = tarj_Marca;
	}
	public String getTarj_Numero() {
		return tarj_Numero;
	}
	public void setTarj_Numero(String tarj_Numero) {
		this.tarj_Numero = tarj_Numero;
	}
	public String getTarj_Token() {
		return tarj_Token;
	}
	public void setTarj_Token(String tarj_Token) {
		this.tarj_Token = tarj_Token;
	}
	public boolean isTarj_Eliminado() {
		return tarj_Eliminado;
	}
	public void setTarj_Eliminado(boolean tarj_Eliminado) {
		this.tarj_Eliminado = tarj_Eliminado;
	}
	public Timestamp getTarj_FechaCreacion() {
		return this.tarj_FechaCreacion;
	}
	public void setTarj_FechaCreacion(Timestamp tarj_FechaCreacion) {
		this.tarj_FechaCreacion = tarj_FechaCreacion;
	}
	public Timestamp getTarj_FechaElimina() {
		return this.tarj_FechaElimina;
	}
	public void setTarj_FechaElimina(Timestamp tarj_FechaElimina) {
		this.tarj_FechaElimina = tarj_FechaElimina;
	}
	
}
