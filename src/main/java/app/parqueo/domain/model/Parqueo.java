package app.parqueo.domain.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

public class Parqueo 
{
	private BigInteger parq_Id;
	private String parq_Placa;
	private Integer vehi_Id;
	private Integer esta_Id;
	private Timestamp parq_FechaInicio;
	private Timestamp parq_FechaFin;
	private String parq_Duracion;
	private BigDecimal parq_Tarifa;
	private BigDecimal parq_Costo;
	
	public BigInteger getParq_Id() {
		return parq_Id;
	}
	public void setParq_Id(BigInteger parq_Id) {
		this.parq_Id = parq_Id;
	}
	public String getParq_Placa() {
		return parq_Placa;
	}
	public void setParq_Placa(String parq_Placa) {
		this.parq_Placa = parq_Placa;
	}
	public Integer getVehi_Id() {
		return vehi_Id;
	}
	public void setVehi_Id(Integer vehi_Id) {
		this.vehi_Id = vehi_Id;
	}
	public Integer getEsta_Id() {
		return esta_Id;
	}
	public void setEsta_Id(Integer esta_Id) {
		this.esta_Id = esta_Id;
	}
	public Timestamp getParq_FechaInicio() {
		return parq_FechaInicio;
	}
	public void setParq_FechaInicio(Timestamp parq_FechaInicio) {
		this.parq_FechaInicio = parq_FechaInicio;
	}
	public Timestamp getParq_FechaFin() {
		return parq_FechaFin;
	}
	public void setParq_FechaFin(Timestamp parq_FechaFin) {
		this.parq_FechaFin = parq_FechaFin;
	}
	public String getParq_Duracion() {
		return parq_Duracion;
	}
	public void setParq_Duracion(String parq_Duracion) {
		this.parq_Duracion = parq_Duracion;
	}
	public BigDecimal getParq_Tarifa() {
		return parq_Tarifa;
	}
	public void setParq_Tarifa(BigDecimal parq_Tarifa) {
		this.parq_Tarifa = parq_Tarifa;
	}
	public BigDecimal getParq_Costo() {
		return parq_Costo;
	}
	public void setParq_Costo(BigDecimal parq_Costo) {
		this.parq_Costo = parq_Costo;
	}
	
	
		
}
