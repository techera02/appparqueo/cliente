package app.parqueo.domain.model;

public class ResponseEliminarVehiculoPorPlaca extends ResponseError {
	private String resultado;

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

}
